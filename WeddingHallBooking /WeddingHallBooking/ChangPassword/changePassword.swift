import UIKit
import KRProgressHUD

class changePassword: UIViewController {
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var email: UITextField!
    
    var headertxt:String = "Change Password"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblHeader.text = headertxt
        email.setLeftPadding(value: 15)
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        
        print(#function)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionAdd(_ sender: UIButton) {
        if (!isValidEmail(email.text)) {
            showErrorMessage(error: "Enter valid email address")
        }else{
            KRProgressHUD.show()
            resetPassword(email: self.email.text!) {
                self.showErrorMessage(title:"Suucess",error: "Success to send you an email to reset you password")
            } failure: { error in
                self.showErrorMessage(error: "\(error.localizedDescription)")
            }
        }
    }
}
