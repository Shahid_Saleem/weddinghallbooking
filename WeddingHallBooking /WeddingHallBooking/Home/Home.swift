import UIKit
import SideMenu
import CoreMIDI
import ToastViewSwift

class Home: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var lblNoRec: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var categories: [categorys] = []
    private var tempcategories: [categorys] = []
    private var searchResultscategories:[categorys] = []
    var menuButton = UIButton()
    let refreshControl = UIRefreshControl()
    
    //searchbar code
    var searchActive : Bool = false
    @IBOutlet weak var searchbar:UISearchBar!
    var subjects = [Array<Any>]()
    var dic = [String: Any]()
    var params : [String : Any] = [:]
    var items: [[String : Any]] = []
    var allHallsNames = [String]()
    var bookCourses =  [categorys]()
    
    var totalData : [[String : Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchbar code
        self.searchbar.showsCancelButton = true
        //searchbar.inputView = UIView.init(frame: CGRect.zero)
        searchbar.inputAccessoryView = UIView.init(frame: CGRect.zero)
        // searchbar.addDoneOnKeyboard(withTarget: self, action: #selector(doneButtonClicked))
        searchbar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "Categories4Cell1", bundle: Bundle.main), forCellReuseIdentifier: "Categories4Cell1")
        loadData()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    // MARK: - Data methods
    func loadData() {
        getCetogryData()
        customNavBar()
    }
    // Customization of navigation bar
    func customNavBar(){
        navigationItem.title = NSLocalizedString("Home", comment: "")
        let sideMenuView = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: sideMenuView.frame.width - 8, height: sideMenuView.frame.height - 8))
        menuButton.center = sideMenuView.center
        menuButton.setImage(UIImage(named: "menu")!, for: .normal)
        menuButton.imageView?.tintColor = .black
        menuButton.addTarget(self, action: #selector(sideMenuClicked), for: .touchUpInside)
        sideMenuView.addSubview(menuButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: sideMenuView)
        
    }
    // to open side menu
    @objc func sideMenuClicked(){
        let SideMenuVc = SideMenuVc()
        present(SideMenuVc, animated: true)
    }
    // MARK: - Refresh methods
    @objc func refresh(_ sender: AnyObject) {
        getCetogryData()
    }
    // To get halls type from firestore
    private func getCetogryData(){
        self.categories.removeAll()
        getCategoryData(){ [weak self] categoriess in
            self?.refreshControl.endRefreshing()
            guard let self = self else { return }
            //print("categoriess.. \(categoriess)")
            self.categories = categoriess
            self.searchResultscategories = categoriess
            self.tableView.reloadData()
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchActive = true
        self.lblNoRec.isHidden = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        self.lblNoRec.isHidden = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.lblNoRec.isHidden = true
        searchbar.text = nil
        searchbar.resignFirstResponder()
        tableView.resignFirstResponder()
        self.searchbar.showsCancelButton = false
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //print("searchtext >> \(String(describing: searchBar.text))")
        searchResultscategories.removeAll()
        self.searchResultscategories = categories
        if searchbar.text == ""{
            searchBar.resignFirstResponder()
        }else if let searchresult = searchbar.text {
            tempcategories = categories
            //Shiraz Arena (Saddar)
            //Sabrina Marquee (Gullbahar)
            
            let result = categories.compactMap {
                $0
            }
            
            
            var uniqueUnordered = [String]()
            do {
                for count in 0...result.count-1{
                    let data = try JSONEncoder().encode(searchResultscategories[count].hallsTypeData)
                    do {
                        let decoder = JSONDecoder()
                        let hallsData = try decoder.decode(hallTypeDatas.self, from: data)
                        hallsData.sorted(by: { a, b  in
                            a.key > b.key
                        }).enumerated().forEach { index, key_value in
                            for i in hallsData {
                                let name = "\(key_value.key)"
                                uniqueUnordered.append(name)
                            }
                        }
                    }catch{
                        print(error)
                    }
                }
                
            }catch{
                print(error)
            }
            
            allHallsNames = Array(Set(uniqueUnordered))
            searchResultscategories.removeAll()
            for item in allHallsNames {
                if item.contains(searchresult){
                    let filtered = result.filter { $0.hallsTypeData.keys.contains(item) }
                    searchResultscategories = filtered
                    self.tableView.reloadData()
                }
            }
            print(result)
        }else{
            let value:Int? = Int(searchBar.text!)
            if 0 ... 500 ~= value! {
                print("No result")
                let toast = Toast.text("No result found")
                toast.show()
            }else if 501 ... 1000 ~= value! {
                for list in categories{
                    print("\(String(describing: list.name))")
                    if list.name == "Silver"{
                        searchResultscategories.append(list)
                        print("searchResultscategories  ??? \(searchResultscategories)")
                    }
                }
            }else if 1001 ... 1500 ~= value! {
                for list in categories{
                    print("\(String(describing: list.name))")
                    if list.name == "Golden"{
                        searchResultscategories.append(list)
                        print("searchResultscategories  ??? \(searchResultscategories)")
                    }
                }
            }else if 1501 ... 2000 ~= value! {
                for list in categories{
                    print("\(String(describing: list.name))")
                    if list.name == "Platinum"{
                        searchResultscategories.append(list)
                        print("searchResultscategories  ??? \(searchResultscategories)")
                    }
                }
            }else {
                print("No resulst")
                let toast = Toast.text("No result found")
                toast.show()
            }
            searchBar.resignFirstResponder()
        }
        searchActive = true
        self.lblNoRec.isHidden = false
        self.searchbar.showsCancelButton = false
        self.tableView.reloadData()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.lblNoRec.isHidden = true
        self.searchActive = true;
        self.searchbar.showsCancelButton = true
        print(searchText)
        
        //MARK: - Search -
        
        searchResultscategories.removeAll()
        self.searchResultscategories = categories
        if searchbar.text == ""{
            searchBar.resignFirstResponder()
        }else if let searchresult = searchbar.text {
            tempcategories = categories
            let result = categories.compactMap {
                $0
            }
            var uniqueUnordered = [String]()
            do {
                for count in 0...result.count-1{
                    let data = try JSONEncoder().encode(searchResultscategories[count].hallsTypeData)
                    do {
                        let decoder = JSONDecoder()
                        let hallsData = try decoder.decode(hallTypeDatas.self, from: data)
                        hallsData.sorted(by: { a, b  in
                            a.key > b.key
                        }).enumerated().forEach { index, key_value in
                            for i in hallsData {
                                let name = "\(key_value.key)"
                                uniqueUnordered.append(name)
                            }
                        }
                    }catch{
                        print(error)
                    }
                }
                
            }catch{
                print(error)
            }
            
            allHallsNames = Array(Set(uniqueUnordered))
            searchResultscategories.removeAll()
            for item in allHallsNames {
                if item.contains(searchresult){
                    let filtered = result.filter { $0.hallsTypeData.keys.contains(item) }
                    searchResultscategories = filtered
                    self.tableView.reloadData()
                }
            }
        }else{
            let value:Int? = Int(searchBar.text!)
            if 0 ... 500 ~= value! {
                print("No result")
                let toast = Toast.text("No result found")
                toast.show()
            }else if 501 ... 1000 ~= value! {
                for list in categories{
                    print("\(String(describing: list.name))")
                    if list.name == "Silver"{
                        searchResultscategories.append(list)
                        print("searchResultscategories  ??? \(searchResultscategories)")
                    }
                }
            }else if 1001 ... 1500 ~= value! {
                for list in categories{
                    print("\(String(describing: list.name))")
                    if list.name == "Golden"{
                        searchResultscategories.append(list)
                        print("searchResultscategories  ??? \(searchResultscategories)")
                    }
                }
            }else if 1501 ... 2000 ~= value! {
                for list in categories{
                    print("\(String(describing: list.name))")
                    if list.name == "Platinum"{
                        searchResultscategories.append(list)
                        print("searchResultscategories  ??? \(searchResultscategories)")
                    }
                }
            }else {
                print("No resulst")
                let toast = Toast.text("No result found")
                toast.show()
            }
            searchBar.resignFirstResponder()
        }
        searchActive = true
        self.lblNoRec.isHidden = false
        self.searchbar.showsCancelButton = false
        self.tableView.reloadData()
        
    }
}

// MARK: - UITableViewDataSource
extension Home: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive{
            self.lblNoRec.isHidden = (searchResultscategories.count) > 0
            return searchResultscategories.count
        }else{
            return categories.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Categories4Cell1", for: indexPath) as! Categories4Cell1
        
        //MARK: - Total Data -
        
        cell.EmployeeClDesign()
        if searchActive{
            cell.labelCategory.text = searchResultscategories[indexPath.row].name
            cell.hallSCountLb.text = "\(searchResultscategories[indexPath.row].hallsTypeData.count)" + "  " + "Halls"
            if searchResultscategories[indexPath.row].id == "1"{
                cell.viewback.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.6117647059, blue: 0.3764705882, alpha: 1)
            }else if searchResultscategories[indexPath.row].id == "2" {
                cell.viewback.backgroundColor = #colorLiteral(red: 1, green: 0.8039215686, blue: 0.3764705882, alpha: 1)
            }else if searchResultscategories[indexPath.row].id == "3" {
                cell.viewback.backgroundColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
            }
        }else{
            cell.labelCategory.text = categories[indexPath.row].name
            cell.hallSCountLb.text = "\(categories[indexPath.row].hallsTypeData.count)" + "  " + "Halls"
            if categories[indexPath.row].id == "1"{
                cell.viewback.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.6117647059, blue: 0.3764705882, alpha: 1)
            }else if categories[indexPath.row].id == "2" {
                cell.viewback.backgroundColor = #colorLiteral(red: 1, green: 0.8039215686, blue: 0.3764705882, alpha: 1)
            }else if categories[indexPath.row].id == "3" {
                cell.viewback.backgroundColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
            }
        }
        return cell
    }
}

// MARK: - UITableViewDelegate

extension Home: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        do {
            if searchActive{
                let data = try JSONEncoder().encode(searchResultscategories[indexPath.row].hallsTypeData)
                UserDefaults.standard.set(data, forKey: "hallsTypeData")
                let filtersCategoryView = FiltersCategoryView()
                filtersCategoryView.type = self.searchResultscategories[indexPath.row].name
                
                navigationController?.pushViewController(filtersCategoryView, animated: true)
            }else{
                let data = try JSONEncoder().encode(categories[indexPath.row].hallsTypeData)
                UserDefaults.standard.set(data, forKey: "hallsTypeData")
                let filtersCategoryView = FiltersCategoryView()
                filtersCategoryView.type = self.categories[indexPath.row].name
                navigationController?.pushViewController(filtersCategoryView, animated: true)
            }
        }catch{
            print(error)
        }
    }
}

extension UIView{
    func EmployeeClDesign (){
        let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 50, 0)
        self.layer.transform = rotationTransform
        self.alpha = 0
        UIView.animate(withDuration: 1.0){
            self.layer.transform = CATransform3DIdentity
            self.alpha = 1.0
        }
    }
}
