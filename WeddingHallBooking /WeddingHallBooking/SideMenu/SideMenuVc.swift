import Firebase
import UIKit
import Kingfisher

class SideMenuVc: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName :UILabel!
    @IBOutlet weak var userEmail :UILabel!
    
    let user = User.currentUser
    var sideMenuItems:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DataManager.sharedInstance.user?.socialType ==  SocialTypes.Google.rawValue {
            let item = ["Home","History"]
            self.sideMenuItems = item
        }else{
            let item = ["Home","History","Change Password"]
            self.sideMenuItems = item
        }
        navigationController?.navigationBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SideMenuCellVc", bundle: Bundle.main), forCellReuseIdentifier: "SideMenuCellVc")
        self.userName.text = user?.name
        self.userEmail.text = user?.email
    }
    
    @IBAction func logOutBtn(_ sender: Any) {
        jumpToNextView()
        GoogleLoginManager.sharedInstance.Logout()
    }
    private func jumpToNextView(){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            let Login1View = Login1View()
            let navigation = NavigationController(rootViewController: Login1View)
            navigation.isModalInPresentation = true
            navigation.navigationBar.isTranslucent = false
            navigation.modalPresentationStyle = .fullScreen
            present(navigation, animated: true)
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
    }
}
extension SideMenuVc: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCellVc", for: indexPath) as? SideMenuCellVc else{
            fatalError("The dequeued cell is not an instance of TableViewCell.")
        }
        cell.titleLabel.textAlignment = .left
        cell.titleLabel.text = sideMenuItems[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let home = Home()
            let navigation = NavigationController(rootViewController: home)
            navigation.isModalInPresentation = true
            navigation.navigationBar.isTranslucent = false
            navigation.modalPresentationStyle = .fullScreen
            present(navigation, animated: true)
            
        }else if indexPath.row == 1 {
            
            let historyy = history()
            present(historyy, animated: true)
            
        }else if indexPath.row == 2 {
            let changePassword = changePassword()
            present(changePassword, animated: true)
        }
    }
}
