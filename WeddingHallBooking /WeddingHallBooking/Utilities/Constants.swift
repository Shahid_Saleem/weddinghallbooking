import Foundation
import UIKit
import SafariServices

var kUserPlaceHolder = UIImage.init(named: "ic_profileDefault")!
let ksettings_single = "ksettings_single"
let kwhatsapp_phone = "kwhatsapp_phone"
let ksms_number = "ksms_number"
let ksearch_history = "ksearch_history"
let kResponseTimeFormat = "yyyy-MM-dd HH:mm:ss"
let kLimitPage = 1000

enum pagerSection: Int {
    case listData
    case loader
}


struct Entity {
    static let productOption = "ProductOption"
    static let cart = "CartItem"
    static let wishlist = "WishList"
    static let user = "user"
}

struct ScreenSize {
    
    static let width        = UIScreen.main.bounds.size.width
    static let height       = UIScreen.main.bounds.size.height
    static let contentHeight = ScreenSize.height - 113 // 20 + 44 + 49 => status, navigation bar, tab bar
    static let maxLength    = max(ScreenSize.width, ScreenSize.height)
    static let minLength    = min(ScreenSize.width, ScreenSize.height)
}

struct ScreenCheck {
    static var kScreenBounds    :   CGRect = UIScreen.main.bounds
    static var isiPhone_4       :   Bool   = 480 == UIScreen.main.bounds.size.height ? true:false
    static var isiPhone_5       :   Bool   = 568 == UIScreen.main.bounds.size.height ? true:false
    static var isiPhone_6       :   Bool   = 667 == UIScreen.main.bounds.size.height ? true:false
    static var isiPhone_6_Plus  :   Bool   = 736 == UIScreen.main.bounds.size.height ? true:false
}

struct DeviceType   {
    static let iPhone   = UIDevice.current.userInterfaceIdiom == .phone
    static let iPad     = UIDevice.current.userInterfaceIdiom == .pad
}
//let vc = self.getVCApp(storyboard: .DashBoard, vcIdentifier: .DashboardVC) as! DashboardVC
//self.pushView(vc)
//self.presentVC(vc,true,showAsPopUp:true)
//self.pushView(self.getTabBar())

//MARK: - Controllers Register
public enum ControllersVC:String {
    
    //-------Auth VC--------
    case SelectedFoodItemsVC = "SelectedFoodItemsVC"
    case SelectDateTimeVC = "SelectDateTimeVC"
    case SelectedBavergesVC = "SelectedBavergesVC"
    case SelectedDesertVC = "SelectedDesertVC"
    
    func getIdentifier() -> String {
        return self.rawValue
    }
}

//MARK: - CollectionViewCell Register
enum CVCells:String ,CaseIterable {
    
    case CategoriesSideMenuCVCell = "CategoriesSideMenuCVCell"
    case TimeCVCell = "TimeCVCell"
    
    var nib:UINib? {
        return UINib.init(nibName: self.rawValue, bundle: nil)
    }
    var identifier:String {
        return self.rawValue
    }
    
    static func addNibs(_ collectionView:UICollectionView){
        for item in CVCells.allCases {
            collectionView.register(item.nib, forCellWithReuseIdentifier: item.identifier)
        }
    }
}

//MARK: - TabelViewCell Register
enum TVCells:String,CaseIterable {
    
    case SelectionServiceTbCell = "SelectionServiceTbCell"
    case SelectedTimeTbCell = "SelectedTimeTbCell"
    case buttonSignUpCell = "buttonSignUpCell"
    
    var nib:UINib? {
        return UINib.init(nibName: self.rawValue, bundle: nil)
    }
    var identifier:String {
        return self.rawValue
    }
    
    static func addNibs(_ tableView:UITableView){
        
        for item in TVCells.allCases {
            tableView.register(item.nib, forCellReuseIdentifier: item.identifier)
        }
    }
}

//MARK: - Storyboard Register
public enum Storyboards :String {
    
    case Home = "Home"
    
    func board()->String{
        return self.rawValue
    }
}
//MARK: - Get VC From Storyborad
extension UIViewController {
    
    public func getVCApp(storyboard : Storyboards, vcIdentifier : ControllersVC) -> UIViewController {
        return UIStoryboard(name: storyboard.board(), bundle: nil).instantiateViewController(withIdentifier: vcIdentifier.rawValue)
    }
}
extension UIViewController :SFSafariViewControllerDelegate{
    
    func openBrowser(link:String){
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = false
        config.barCollapsingEnabled = true
        if let url = URL(string: link){
            let vc = SFSafariViewController(url: url, configuration: config)
            vc.dismissButtonStyle = .close
            vc.delegate = self
            self.present(vc, animated: true)
        }
    }
    
    @objc public func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        
    }
    
    class func controllerFromSB()->UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self))
    }
    
    func hideNavigationBar(){
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func showNavigationBar(){
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    public func pushView( vc:UIViewController?, isAnimated:Bool = true,isFromTabControllerPush:Bool = false){
        if let vc = vc {
            if(isFromTabControllerPush){
                self.tabBarController?.navigationController?.pushViewController(vc, animated: isAnimated)
            }else{
                self.navigationController?.pushViewController(vc, animated: isAnimated)
            }
        }
    }
    
    public func presentVC( vc:UIViewController?, isAnimated:Bool = true,showAsPopUp:Bool = false,isFromTabControllerPush:Bool = false){
        if showAsPopUp{
            vc?.modalPresentationStyle = .overCurrentContext
            vc?.modalTransitionStyle = .crossDissolve
        }
        if let vc = vc {
            if(isFromTabControllerPush){
                if let tab = self.tabBarController,let nav = tab.navigationController{
                    nav.present(vc, animated: isAnimated, completion: nil)
                }else{
                    self.present(vc, animated: isAnimated, completion: nil)
                }
            }else{
                if let nav = self.navigationController{
                    nav.present(vc, animated: isAnimated, completion: nil)
                }else{
                    self.present(vc, animated: isAnimated, completion: nil)
                }
            }
        }
    }
    
    public func presentAlertVC( vc:UIViewController?, isAnimated:Bool = true,isFromTabControllerPush:Bool = false){
        if let vc = vc {
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            if(isFromTabControllerPush){
                self.tabBarController?.navigationController?.present(vc, animated: isAnimated, completion: nil)
            }else{
                self.navigationController?.present(vc, animated: isAnimated, completion: nil)
            }
        }
    }
}

extension NSLayoutConstraint {
    func setMultiplier(_ multiplier:CGFloat) -> NSLayoutConstraint {
        NSLayoutConstraint.deactivate([self])
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
    public  func setRelation( relation:NSLayoutConstraint.Relation, constant:CGFloat) -> NSLayoutConstraint {
        NSLayoutConstraint.deactivate([self])
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension UITableView {
    public func registerNib(nibName: String, identifier: String? = nil) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: identifier ?? nibName)
    }
}
extension UICollectionView {
    public func registerNib(nibName: String, identifier: String? = nil) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifier ?? nibName)
    }
}

extension UICollectionView {
    
    func toggleActivityIndicator() {
        if let indicator = backgroundView as? UIActivityIndicatorView {
            indicator.stopAnimating()
            indicator.removeFromSuperview()
        }else{
            let indicator = UIActivityIndicatorView()
            indicator.style = .whiteLarge
            indicator.color = .black
            indicator.hidesWhenStopped = true
            backgroundView = indicator
            indicator.startAnimating()
        }
    }
}
extension UIView {
    static var identifier: String {
        return String(describing: self)
    }
}
extension String{
    func toDate( dateFormat format  : String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.date(from: self)!
    }
    
    func UTCToLocal(inputFormate : String , outputFormate : String) -> String {
        if self.count > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  inputFormate  //Input Format kResponseTimeFormat
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let UTCDate = dateFormatter.date(from: self)
            dateFormatter.dateFormat =  outputFormate // Output Format "MM.dd.yyyy hh:mm a"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let UTCToCurrentFormat = dateFormatter.string(from: UTCDate ?? Date())
            print(UTCToCurrentFormat)
            return UTCToCurrentFormat
        }else{
            return ""
        }
    }
    func getRanges(of string: String) -> [NSRange] {
        var ranges:[NSRange] = []
        if contains(string) {
            let words = self.components(separatedBy: " ")
            var position:Int = 0
            for word in words {
                if word.lowercased() == string.lowercased() {
                    let startIndex = position
                    let endIndex = word.count
                    let range = NSMakeRange(startIndex, endIndex)
                    ranges.append(range)
                }
                position += (word.count + 1)
            }
        }
        return ranges
    }
    
    func getRemidersRemainingDays() -> String{
        let dateRangeStart = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kResponseTimeFormat
        let dt = dateFormatter.date(from: self)
        //        dt = dt?.toLocalTime()
        let calendar = Calendar.current
        if calendar.isDateInTomorrow(dt!) {
            return "Tomorrow"
        }else if  calendar.isDateInToday(dt!){
            return "Today"
        }else{
            var diffInDays = calendar.dateComponents([.day], from: dateRangeStart, to: dt!).day
            if diffInDays! > 0 {
                diffInDays = (diffInDays!) + 1
            }
            if diffInDays! <= 0{
                return "Today"
            }else{
                return "\(String(describing: diffInDays!)) Days"
            }
        }
    }
}
