import UIKit


//let vc = self.getVCApp(storyboard: .DashBoard, vcIdentifier: .DashboardVC) as! DashboardVC
//vc.hidesBottomBarWhenPushed = true
//self.pushView(vc:vc)
//self.presentVC(vc,true,showAsPopUp:true)
//CustomLoader.shared.show()

class BaseVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //MARK: - IBAction
    @IBAction public func goBack(_ sender : UIButton){
        if self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}
//MARK: - Navigation
extension UINavigationController {
    func popToViewController(ofClass: AnyClass, animated: Bool = true) {
        if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
            popToViewController(vc, animated: animated)
        }
    }
}
