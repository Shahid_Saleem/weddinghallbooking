//
// Copyright (c) 2021 Related Code - https://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit

//-----------------------------------------------------------------------------------------------------------------------------------------------
extension UIView {

	//-------------------------------------------------------------------------------------------------------------------------------------------
	@IBInspectable
	var cornerRadius: CGFloat {

		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	@IBInspectable
	var borderWidth: CGFloat {

		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}

    @IBInspectable
	var borderColor: UIColor? {

		get {
			let color = UIColor.init(cgColor: layer.borderColor!)
			return color
		}
		set {
			layer.borderColor = newValue?.cgColor
		}
	}
}

public func customButtons ( customButton : UIButton ,shadowColor : UIColor , width : Int , height : Int , shadowRadius : CGFloat ,shadowOpacity :Float ,
                           btnName :String  ,backgrounColor :UIColor ,textColor : UIColor){
    DispatchQueue.main.async {

        customButton.backgroundColor = backgrounColor
        customButton.tintColor = textColor

        customButton.layer.shadowColor = shadowColor.cgColor
        customButton.layer.shadowOffset = CGSize(width: width, height: height)
        customButton.layer.shadowRadius = shadowRadius
        customButton.layer.shadowOpacity = shadowOpacity
        customButton.setTitle(btnName, for: .normal)

    }
}

public extension UIViewController {
    func showErrorMessage(title:String = "Alert",error:String) {
        let alert = UIAlertController.init(title: title, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
            
        }
    }
}
extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}
