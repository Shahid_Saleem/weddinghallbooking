import UIKit
import Firebase
import GoogleSignIn
let GOOGLE_CLIENT_ID                = "62070331048-u699rrtkev59m24dl068mo2i0c649o65.apps.googleusercontent.com"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var restrictRotation:UIInterfaceOrientationMask = .portrait
    var window: UIWindow?
    var login1View: Login1View!
    static var shared = AppDelegate()
    var rootVC : UIViewController?
    
    //-------------------------------------------------------------------------------------------------------------------------------------------
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        
        
        
        
        
        //----------------------------------------------------------------------------------------------------------------------------
        // UI initialization
        //---------------------------------------------------------------------------------------------------------------------------------------
        window = UIWindow(frame: UIScreen.main.bounds)
        
        login1View = Login1View(nibName: "Login1View", bundle: nil)
        let navController = NavigationController(rootViewController: login1View)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        FirebaseApp.configure()
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.restrictRotation
    }

    //-------------------------------------------------------------------------------------------------------------------------------------------
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------------------
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------------------
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------------------
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------------------
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

extension AppDelegate {
    
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print(url as Any)
        return GIDSignIn.sharedInstance.handle(url)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance.handle(url)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "GoogleAuthNotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
        }else{
            // [START_EXCLUDE]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "GoogleAuthNotification"),
                object: nil,
                userInfo: ["user": user as Any])
            // [END_EXCLUDE]
        }
    }
    // [END signin_handler]
    // [START disconnect_handler]
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "GoogleAuthNotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }
}
