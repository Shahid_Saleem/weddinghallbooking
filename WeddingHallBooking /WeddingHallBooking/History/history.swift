import UIKit
import Kingfisher

class history: UIViewController {
    
    @IBOutlet weak var hallName:UILabel!
    @IBOutlet weak var hallAddress:UILabel!
    @IBOutlet weak var hallImage:UIImageView!
    @IBOutlet weak var reservationData:UILabel!
    @IBOutlet weak var hallfood: UILabel!
    @IBOutlet weak var hallDesert: UILabel!
    @IBOutlet weak var hallService: UILabel!
    @IBOutlet weak var hallDate: UILabel!
    @IBOutlet weak var hallPrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getReservationData()
    }
    private func getReservationData(){
        if let uid = getCurrentUserId() {
            getRersevationOfClient(
                byId: uid,
                success: { [weak self] currentReservation in
                    Reservation.currentReservation = currentReservation
                   // let reservationDate =  UserDefaults.standard.value(forKey: "bookingDate")
                    self?.hallName.text = "Name :   \(currentReservation?.name ?? "")"
                    self?.hallAddress.text = "Address :  \(currentReservation?.address ?? "")"
                    self?.reservationData.text = "Booking Date :  \(currentReservation?.reservationDate ?? "")"
                    self?.hallfood.text = "Food :   \(currentReservation?.food ?? "")"
                    self?.hallDesert.text = "Desert :   \(currentReservation?.desert ?? "")"
                    self?.hallService.text = "Service :   \(currentReservation?.service ?? "")"
                    self?.hallPrice.text = currentReservation?.price
                    let url = URL(string: currentReservation?.imageUrl ?? "")
                    if url != nil {
                        self?.hallImage.loadGif(url: url, placeholder: PlaceHolder.icPlaceholder, mode: .scaleAspectFill)
                    }
                },
                failure: { [weak self] in
                    self?.showErrorMessage(error: "Faild get data")
                }
            )
        }else{
            self.showErrorMessage(error: "Faild get data")
        }
    }
}
