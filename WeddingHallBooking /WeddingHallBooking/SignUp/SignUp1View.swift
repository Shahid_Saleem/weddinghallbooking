import UIKit
import KRProgressHUD
import Alamofire

class SignUp1View: UIViewController {
    
    @IBOutlet var textFieldLastName: UITextField!
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPassword: UITextField!
    @IBOutlet var textFieldPassword2: UITextField!
    @IBOutlet weak var showHidPasswordOne: UIButton!
    @IBOutlet weak var showHidPasswordTwo: UIButton!
    @IBOutlet var textFieldNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.titleView = UIImageView(image: UIImage(systemName: "circles.hexagongrid.fill"))
        textFieldLastName.setLeftPadding(value: 15)
        textFieldEmail.setLeftPadding(value: 15)
        textFieldPassword.setLeftPadding(value: 15)
        textFieldPassword2.setLeftPadding(value: 15)
        textFieldNumber.setLeftPadding(value: 15)
    }
    
    @IBAction func actionHideShowPassword(_ sender: Any) {
        showHidPasswordOne.isSelected = !showHidPasswordOne.isSelected
        textFieldPassword.isSecureTextEntry = !showHidPasswordOne.isSelected
    }
    
    @IBAction func actionHideShowPassword2(_ sender: Any) {
        showHidPasswordTwo.isSelected = !showHidPasswordTwo.isSelected
        textFieldPassword2.isSecureTextEntry = !showHidPasswordTwo.isSelected
    }
    
    @IBAction func actionContinue(_ sender: Any) {
        checkFiledsValidation(email: textFieldEmail.text!, password: textFieldPassword.text!)
    }
    
    private func checkFiledsValidation(email :String,password:String){
        let name = textFieldLastName.text ?? ""
        let password1 = textFieldPassword.text ?? ""
        let password2 = textFieldPassword2.text ?? ""
        let phone = textFieldNumber.text ?? ""
        
        if (name.isEmpty) {
            showErrorMessage(error: "Enter your Name")
        }else if (!isValidEmail(textFieldEmail.text)) {
            showErrorMessage(error: "Enter valid email address")
        }else if phone.isEmpty{
            showErrorMessage(error: "Enter your Phonenumber")
        }else if password1.isEmpty{
            showErrorMessage(error: "Enter Password")
        }else if password2.isEmpty{
            showErrorMessage(error: "Enter Conferm Password")
        }else{
            if textFieldPassword.text! != textFieldPassword2.text! {
                self.showErrorMessage(error: "Sorry, your Passwords is not matching.")
            }else{
                KRProgressHUD.show()
                emailRegister(emailText: email, passwortText: password, success: { [weak self] in
                    if email != "", password != "" , self?.textFieldLastName.text != "" , getCurrentUserId() != nil {
                        if let uid = UserDefaults.standard.string(forKey: "uid") {
                            let data: [String:Any] = ["name" :self?.textFieldLastName.text! as Any,"email": email, "password": password, "id": uid, "signUpDate": TimeFormatHelper.string(for: Date(), format: "MM-dd-yyyy"),"phoneNumber" :self?.textFieldNumber.text! as Any]
                            User.currentUser = parseUser(jsonUser: data)
                            setUser(id: uid, userData: data)
                            KRProgressHUD.dismiss()
                            self!.jumpToNextView()
                        }else{
                            KRProgressHUD.dismiss()
                            self?.showErrorMessage(error: "Faild to sign up")
                        }
                    }else{
                        KRProgressHUD.dismiss()
                        self?.showErrorMessage(error: "Please fill all fields")
                    }
                }, failure: { error in
                    KRProgressHUD.dismiss()
                    self.showErrorMessage(error: "\(error.localizedDescription )")
                })
            }
        }
    }
    
    private func jumpToNextView(){
        let loginView = Login1View()
        let navigation = NavigationController(rootViewController: loginView)
        navigation.isModalInPresentation = true
        navigation.navigationBar.isTranslucent = false
        navigation.modalPresentationStyle = .fullScreen
        present(navigation, animated: true)
        
    }
    
    @IBAction func signIn(_ sender: Any) {
        let login1View = Login1View()
        login1View.isModalInPresentation = true
        login1View.modalPresentationStyle = .fullScreen
        present(login1View, animated: true)
    }
}

