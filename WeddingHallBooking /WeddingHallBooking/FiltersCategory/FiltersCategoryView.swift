import UIKit

class FiltersCategoryView: UIViewController {
    
    var hallsData = hallTypeDatas()
    var hallDataDetails = hallTypeData()
    var type:String = ""
    var searchedText: String = ""
    var filteredData = hallTypeDatas()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = type
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .never
        getHallsDataa()
        
    }
    
    private func getHallsDataa(){
        // To get the hallsType data that saved in the home view controller by decoding
        if let data = UserDefaults.standard.data(forKey: "hallsTypeData") {
            do {
                let decoder = JSONDecoder()
                let hallsData = try decoder.decode(hallTypeDatas.self, from: data)
                self.hallsData = hallsData
                let buttonHeight = 100
                let historyScrollView = UIScrollView(frame: CGRect(x: 0, y: 10, width: 500, height:800))
                historyScrollView.showsVerticalScrollIndicator = false
                historyScrollView.showsHorizontalScrollIndicator = false
                historyScrollView.contentSize = CGSize(width: 400, height: 800)
                hallsData.sorted(by: { a, b  in
                    a.key > b.key
                }).enumerated().forEach { index, key_value in
                    let hallsDataLbl = hallsDetailsData(frame: CGRect(x: 30, y: CGFloat(5 + ((buttonHeight + 5) * index)), width: 320, height: CGFloat(buttonHeight)))
                    var add:String = ""
                    for i in self.hallsData {
                        add = i.value.hallName
                    }
                    hallsDataLbl.setTitle("\(key_value.key)", for: .normal)
                    hallsDataLbl.backgroundColor = .darkGray
                    hallsDataLbl.cornerRadius = 10
                    hallsDataLbl.hallTypeDatas = key_value.value
                    hallsDataLbl.addTarget(self, action: #selector(self.goToHallsDetails(_:)), for: .touchUpInside)
                    historyScrollView.addSubview(hallsDataLbl)
                }
                self.view.addSubview(historyScrollView)
            }catch{
                print(error)
            }
        }
    }
    // Here we go to another view controller that contains the halls details
    @objc func goToHallsDetails(_ sender: hallsDetailsData) {
        hallDataDetails = sender.hallTypeDatas!
        do {
            let data = try JSONEncoder().encode(sender.hallTypeDatas!)
            UserDefaults.standard.set(data, forKey: "hallDataDetails")
            let item3DTouchView = Item3DTouchView()
            item3DTouchView.type = type
            self.navigationController?.pushViewController(item3DTouchView, animated: true)
            
        }catch{
            print(error)
        }
    }
    class hallsDetailsData: UIButton {
        var hallTypeDatas: hallTypeData?
    }
}
