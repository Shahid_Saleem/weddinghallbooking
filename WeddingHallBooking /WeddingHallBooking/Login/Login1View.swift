import UIKit
import KRProgressHUD
import GoogleSignIn

class Login1View: UIViewController {
    
    
    //MARK: - IBOutlets
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelSubTitle: UILabel!
    @IBOutlet var imageViewLogo: UIImageView!
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPassword: UITextField!
    @IBOutlet var buttonHideShowPassword: UIButton!
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.sharedInstance.logoutUser()
        self.textFieldEmail.text = "alibangash457@gmail.com"
        self.textFieldPassword.text = "12345678"
        textFieldEmail.setLeftPadding(value: 15)
        textFieldPassword.setLeftPadding(value: 15)
        textFieldPassword.setRightPadding(value: 40)
        loadData()
    }
    //MARK: - Function
    func loadData() {
        labelTitle.text = "Welcome to\nWedding Hall Booking"
        labelSubTitle.text = "A best place for the whole brides to book their wedding hall."
    }
    private func jumpToNextView(){
        let home = Home()
        let navigation = NavigationController(rootViewController: home)
        navigation.isModalInPresentation = true
        navigation.navigationBar.isTranslucent = false
        navigation.modalPresentationStyle = .fullScreen
        present(navigation, animated: true)
    }
    func loginApi(){
        KRProgressHUD.show()
        emailLogin(emailText: textFieldEmail.text, passwordText: textFieldPassword.text, success: { [weak self] in
            if let uid = getCurrentUserId() {
                getUser(
                    byId: uid,
                    success: { [weak self] currentUser in
                        User.currentUser = currentUser
                        KRProgressHUD.dismiss()
                        self?.jumpToNextView()
                    },
                    failure: { [weak self] in
                        KRProgressHUD.dismiss()
                        self?.showErrorMessage(error: "Something Went to Wrong")
                    }
                )
            }else{
                KRProgressHUD.dismiss()
                self?.showErrorMessage(error: "Something Went to Wrong")
            }
            
        }, failure: { error in
            KRProgressHUD.dismiss()
            self.showErrorMessage(error: "\(error.localizedDescription )")
        })
    }
    // MARK: - User IBAction
    @IBAction func forgotAction(_ sender: Any) {
        let changePassword = changePassword()
        changePassword.headertxt = "Forgot Password"
        present(changePassword, animated: true)
    }
    //MARK: Google Login Action
    @IBAction func googleSignIn(_ sender: Any) {
        GoogleLoginManager.sharedInstance.LoginGoogle(googleDelegate: self)
    }
    //End Google
    @IBAction func actionHideShowPassword(_ sender: Any) {
        buttonHideShowPassword.isSelected = !buttonHideShowPassword.isSelected
        textFieldPassword.isSecureTextEntry = !buttonHideShowPassword.isSelected
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        // Validation & Fcm Call
        let password = self.textFieldPassword.text ?? ""
        if (!isValidEmail(textFieldEmail.text)) {
            showErrorMessage(error: "Enter valid email address")
        }else if password.isEmpty{
            showErrorMessage(error: "Enter Password")
        }else{
            self.loginApi()
        }
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        let signUp1View = SignUp1View()
        let navigation = NavigationController(rootViewController: signUp1View)
        navigation.isModalInPresentation = true
        navigation.navigationBar.isTranslucent = false
        navigation.modalPresentationStyle = .fullScreen
        present(navigation, animated: true)
        
    }
}

extension Login1View : GoogleLoginDelegate {
    func googleLoginSuccess(user: UserModel) {
        let old = User(withName: "\(user.first_name ?? "") \(user.last_name ?? "")", id: "\(user.id ?? 0)", signUpDate: Date(), email: user.email ?? "", password: "", phoneNumber: user.phone ?? "")
        User.currentUser = old
        DataManager.sharedInstance.user = user
        DataManager.sharedInstance.saveUserPermanentally(user)
        self.jumpToNextView()
        
    }
    
    func googleLoginFailed(error: String) {
        
    }
}
