import Foundation
import Firebase
import KRProgressHUD


func isValidEmail(_ email: String?) -> Bool {
    if (email == nil) {
        return false
    }
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}

func setCurrentUser(uid: String, isProfessional:Bool = false, success:@escaping(()->()) = {}, failure:@escaping(()->()) = {}) {
    
    getUser(byId: uid, success: { currentUser in
        User.currentUser = currentUser
        success()
    }, failure: failure)
}

func firebaseLogin(withToken token:String, success: @escaping(()->()), failure:((Error)->())? = nil) {
    Auth.auth().signIn(withCustomToken: token) { result, error in
        if error == nil {
            success()
        } else {
            failure?(error!)
        }
    }
}

// TODO Handle two-factor auth
func firebaseLogin(credential: AuthCredential, isProfessional:Bool = false, isFirstTime: Bool = false, success: @escaping(()->()), failure:((Error)->())? = nil) {
    Auth.auth().signIn(with: credential) { authResult, error in
        if error != nil {
            let authError = error! as NSError
            
            if  (AuthErrorCode.secondFactorRequired.rawValue != 0) {
                // The user is a multi-factor user. Second factor challenge is required.
                let resolver = authError
                    .userInfo[AuthErrorUserInfoMultiFactorResolverKey] as! MultiFactorResolver
                var displayNameString = ""
                for tmpFactorInfo in resolver.hints {
                    displayNameString += tmpFactorInfo.displayName ?? ""
                    displayNameString += " "
                }
                
            } else {
                failure?(error!)
                return
            }
            // success
            return
        }
        // User is signed in
        authResult?.user.getIDToken(completion: { token, err in
            if err == nil {
                UserDefaults.standard.set(authResult?.user.uid, forKey: isProfessional ? "pid" : "uid")
                UserDefaults.standard.set(token, forKey: "accessToken")
            } else {
                failure?(error!)
                NSLog(err?.localizedDescription ?? "SOME ERROR")
            }
        })
        if isFirstTime {
            success()
            return
        }
        if let uid = authResult?.user.uid {
            setCurrentUser(uid: uid, isProfessional: isProfessional, success: success, failure: success)
        }
    }
}

func getCurrentCredential() -> String? {
    return UserDefaults.standard.value(forKey: "accessToken") as? String
}

func emailLogin(emailText: String?, passwordText: String?, success: @escaping(()->()), failure:((Error)->())?=nil) {
    
    Auth.auth().signIn(withEmail: emailText!, password: passwordText!) { authResult, error in
        if error == nil {
            authResult?.user.getIDToken(completion: { token, err in
                if err == nil {
                    UserDefaults.standard.set(authResult?.user.uid, forKey: "uid")
                    UserDefaults.standard.set(token, forKey: "accessToken")
                    success()
                }else{
                    failure?(err!)
                    NSLog(err?.localizedDescription ?? "SOME ERROR")
                }
            })
            
        }else{
            failure?(error!)
            NSLog(error?.localizedDescription ?? "SOME ERROR")
        }
    }
}

func emailRegister(emailText: String?, passwortText: String?, isProfessional:Bool = false,  success: @escaping(()->()),failure:((Error)->())?=nil){
    
    Auth.auth().createUser(withEmail: emailText!, password: passwortText!) {authResult, error in
        if error == nil {
            KRProgressHUD.dismiss()
            authResult?.user.getIDToken(completion: { token, err in
                if err == nil {
                    UserDefaults.standard.set(authResult?.user.uid, forKey: isProfessional ? "pid" : "uid")
                    UserDefaults.standard.set(token, forKey: "accessToken")
                    success()
                }else{
                    KRProgressHUD.dismiss()
                    NSLog(err?.localizedDescription ?? "SOME ERROR")
                }
            })
        }else{
            failure?(error!)
            let errorr = error?.localizedDescription ?? "SOME ERROR"
            KRProgressHUD.dismiss()
            NSLog(errorr)
        }
    }
}

func resetPassword(email :String ,success: @escaping(()->()),failure:((Error)->())?=nil){
    Auth.auth().sendPasswordReset(withEmail: email) { (error) in
        if error == nil {
            success()
            KRProgressHUD.dismiss()
        }else{
            failure?(error!)
            KRProgressHUD.dismiss()
            let errorr = error?.localizedDescription ?? "SOME ERROR"
            NSLog(errorr)
        }
    }
}

func logOut(success: @escaping(()->()), failure: (()->())? = nil){
    let firebaseAuth = Auth.auth()
    do {
        try firebaseAuth.signOut()
        UserDefaults.standard.removeObject(forKey: "uid")
        UserDefaults.standard.removeObject(forKey: "pid")
        UserDefaults.standard.removeObject(forKey: "accessToken")
        User.currentUser = nil
        //        Professional.currentProfessional = nil
        success()
    } catch let signOutError as NSError {
        print("Error signing out: %@", signOutError)
        failure?()
    }
}

func getCurrentUserId() -> String? {
    return UserDefaults.standard.string(forKey: "uid")
}

func getCurrentId() -> String? {
    return getCurrentUserId() ?? ""
}

