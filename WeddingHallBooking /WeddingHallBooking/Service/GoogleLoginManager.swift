import Foundation
import GoogleSignIn
import AuthenticationServices

@objc protocol GoogleLoginDelegate :AnyObject{
    @objc func googleLoginSuccess(user : UserModel)
    @objc func googleLoginFailed(error : String)
}

class GoogleLoginManager: NSObject {
    static let sharedInstance = GoogleLoginManager()
    var delegate : GoogleLoginDelegate?
    var access_token : String  = ""
    func LoginGoogle(googleDelegate : UIViewController){
        self.delegate =  (googleDelegate as! GoogleLoginDelegate)
        //Mark : Receive google singin notificaiton
        NotificationCenter.default.addObserver(self,selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "GoogleAuthNotification"),
                                               object: nil)
        let googleUser =   GIDSignIn.sharedInstance.currentUser
        if googleUser != nil {
            access_token = (googleUser?.authentication.accessToken)!
            delegate?.googleLoginSuccess(user: UserModel().genrateUserFromGoogle(googleUser: googleUser))
            return
        }
        
        GIDSignIn.sharedInstance.signIn(
            with: .init(clientID: GOOGLE_CLIENT_ID),
            presenting: googleDelegate
        ) { user, error in
            guard error == nil else {
                self.delegate?.googleLoginFailed(error: "Unable to login with Google!")
                return
                
            }
            guard let user = user else {
                self.delegate?.googleLoginFailed(error: "Unable to login with Google!")
                return
                
            }
            self.delegate?.googleLoginSuccess(user: UserModel().genrateUserFromGoogle(googleUser: user))
            // Your user is signed in!
        }
    }
    
    func Logout() {
        GIDSignIn.sharedInstance.signOut()
        DataManager.sharedInstance.logoutUser()
    }
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "GoogleAuthNotification" {
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:Any] else {
                    self.delegate?.googleLoginFailed(error: "Unable to login with Google!")
                    return
                }
                if let google_user : GIDGoogleUser = userInfo["user"] as? GIDGoogleUser{
                    delegate?.googleLoginSuccess(user: UserModel().genrateUserFromGoogle(googleUser: google_user))
                }else{
                    delegate?.googleLoginFailed(error: "Unable to login with Google!")
                }
            }else{
                delegate?.googleLoginFailed(error: "Unable to login with Google!")
            }
        }else{
            delegate?.googleLoginFailed(error: "Unable to login with Google!")
        }
    }
}

class UserModel :NSObject, Codable{
    
    var socialType:String? = ""
    var social_id:String? = ""
    var social_type:String? = ""
    var created_at:String? = ""
    var updated_at:String? = ""
    var phone:String? = ""
    var token:String? = ""
    var id : Int? = 0
    var first_name:String? = ""
    var last_name:String? = ""
    var fullName:String {
        return "\(self.first_name ?? "") \(self.last_name ?? "")".trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var pic:String? = ""
    var email:String? = ""
    var is_social:Int? = 0
    
    func genrateUserFromGoogle(googleUser :  GIDGoogleUser?) -> UserModel {
        let userObj = UserModel()
        userObj.token =  googleUser?.authentication.accessToken ?? ""
        let googleUsername = googleUser?.profile?.name ?? ""
        let firstname = googleUsername.components(separatedBy: " ").first
        let lastname = googleUsername.components(separatedBy: " ").last
        userObj.token = googleUser?.userID ?? ""
        userObj.first_name = firstname?.capitalized
        userObj.last_name = lastname?.capitalized
        userObj.email = googleUser?.profile?.email ?? ""
        if userObj.email?.isEmpty == true{
            userObj.email = "\(userObj.token ?? "")@google.com"
        }
        userObj.pic = googleUser?.profile?.imageURL(withDimension: 1024)?.absoluteString ?? ""
        userObj.socialType = SocialTypes.Google.rawValue
        return userObj
    }
    
}

enum SocialTypes:String,CaseIterable {
    
    
    case Facebook = "facebook"
    case Google = "google"
    case Apple = "apple"
    case None = ""
}
