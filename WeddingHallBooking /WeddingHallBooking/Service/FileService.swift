import FirebaseStorage

func UploadImageToFirestorage(fileUrl: URL , childPath : String, success: ((String)->())? = nil, failure: ((String?)->())? = nil)  {
    let storage = Storage.storage()
    let storageRef = storage.reference()
    let localFile = fileUrl

    let photoRef = storageRef.child(childPath)
    _ = photoRef.putFile(from: localFile, metadata: nil) {(metadata , error) in
        guard metadata != nil else {
            print(error?.localizedDescription ?? "Image not uploaded")
            failure?(error?.localizedDescription)
            return
        }
        if let url = metadata?.path {
            success?(url)
        }
    }
}
func GetImageFromFireStorage(image : UIImageView , childPath : String , imageUrl:URL){
    // TODO: get image from storage
  }
