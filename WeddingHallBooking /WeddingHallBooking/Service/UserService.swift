import Foundation

func saveCurrentUser(success: (()->())? = nil) {
    if User.currentUser == nil {
        return
    }
    setUser(id: User.currentUser!.id ?? "", userData: jsonUser(user: User.currentUser!), success: success)
}

func updateCurrentUser(success:(()->())? = nil) {
    if User.currentUser == nil {
        return
    }
    getUser(byId: User.currentUser!.id ?? "") { user in
        User.currentUser = user
        success?()
    }
}

func getUser(byId id: String, success: @escaping((User?)->()), failure: @escaping(()->()) = {}) {
    getDocument(collection: "users", id: id, success: { itemData in
        let user = parseUser(jsonUser: itemData)
        success(user)
    }, failure: failure)
}

func setUser(id: String, userData: [String: Any], success: (()->())? = nil) {
    addToFireStore(collection: "users", docName: id, data: userData, success: success)
}

func setReservation(id: String, reservationData: [String: Any], success: (()->())? = nil) {
    addToFireStore(collection: "history", docName: id, data: reservationData, success: success)
}

func getCategoryData( success: @escaping(([categorys])->())) {
    getDocuments(collection: "hallTypes") { jsonCatogrys in
        let category: [categorys?] = jsonCatogrys.map{ jsonCatogrysss -> categorys? in
            parseCatogry(jsonCatogry: jsonCatogrysss)
        }
        let categoryList: [categorys] = category.compactMap{ $0 }
        success(categoryList)
    }
}

func saveBookingData(success: (()->())? = nil) {
    if User.currentUser == nil {
        return
    }
    setReservation(id: User.currentUser!.id ?? "", reservationData: jsonReservation(reservation: Reservation.currentReservation!), success: success)
}
func getRersevationOfClient(byId id: String, success: @escaping((Reservation?)->()), failure: @escaping(()->()) = {}) {
    getDocument(collection: "history", id: id, success: { itemData in
        let reservation = parseReservationData(jsonReservation: itemData)
        print("reservation... \(reservation)")
        success(reservation)
    }, failure: failure)
}
