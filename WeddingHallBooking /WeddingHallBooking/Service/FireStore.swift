import Foundation
import Firebase


let db = Firestore.firestore()

func addToFireStore(collection: String, docName: String? = nil, data: [String : Any], success: (()->())? = nil) {
    if docName == nil {
        db.collection(collection).addDocument(data: data) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("New document added")
                success?()
            }
        }
        return
    }
    db.collection(collection).document(docName!).setData(data) { err in
        if let err = err {
            print("Error adding document: \(err)")
        } else {
            print("Document added with ID: \(docName!)")
            success?()
        }
    }
}

func SetForestoreRecord(collection: String, docName: String, data: [String : Any], success: (()->())? = nil, failure: (()->())? = nil) {
    db.collection(collection).document(docName).setData(data) { err in
        if let err = err {
            print("Error adding document: \(err)")
            failure?()
        } else {
            print("New document added")
            success?()
        }
    }
}

func getDocument(collection: String, id: String, success: @escaping([String : Any]?)->(), failure: (()->())? = nil)
{
    db.collection(collection).document(id).getDocument {(document, error) in
        if let document = document, document.exists {
            let itemData = document.data()
            success(itemData)
        } else {
            failure?()
            print("\(error.debugDescription)")
        }
        
    }
}

func getDocuments(collection: String, success: @escaping([[String : Any]])->()) {
    let query = db.collection(collection)
    getArrayFromDocs(query: query, success: success)
    
}

func getDocuments(collection: String, field: String, value: Any, success: @escaping([[String : Any]])->(), failure: (()->())? = nil) {
    let query = db.collection(collection).whereField(field, isEqualTo: value)
    getArrayFromDocs(query: query, success: success, failure: failure)
}

func getDocuments(collection: String, field: String, containsValue: Any, success: @escaping([[String : Any]])->(), failure: (()->())? = nil) {
    let query = db.collection(collection).whereField(field, arrayContains: containsValue)
    getArrayFromDocs(query: query, success: success, failure: failure)
}

func addToDocArray(collection: String, document: String, arrayField: String, value: [Any]) {
    db.collection(collection).document(document).updateData([
        arrayField: FieldValue.arrayUnion(value)
    ])
}

func removeFromDocArray(collection: String, document: String, arrayField: String, value: [Any]) {
    db.collection(collection).document(document).updateData([
        arrayField: FieldValue.arrayRemove(value)
    ])
}

func getArrayFromDocs(query: Query, success: @escaping([[String : Any]])->(), failure: (()->())? = nil) {
    query.getDocuments() { (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
            failure?()
        } else {
            var items: [[String : Any]] = []
            for document in querySnapshot!.documents {
                if document.exists {
                    let itemData = document.data()
                        items.append(itemData)
                } else {
                    print("Document does not exist")
                }
            }
            if (items.count > 0) {
                success(items)
            } else {
                failure?()
            }
        }
    }
}

func deleteDocument(collection: String, id: String, success: (()->())? = nil, failure:((Error)->())? = nil) {
    db.collection(collection).document(id).delete() { err in
        if let err = err {
            failure?(err)
            print("Error removing document: \(err)")
        } else {
            success?()
            print("Document with id \(id) successfully removed!")
        }
    }
}
