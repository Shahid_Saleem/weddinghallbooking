import UIKit

class buttonSignUpCell: UITableViewCell {

    @IBOutlet weak var btnTop: NSLayoutConstraint! 
    @IBOutlet weak var btnRegister: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    var onClickMe:(() -> Void)?

    @IBAction func signUpButtonAction(_ sender: Any) {
        self.onClickMe?()
    }
}
