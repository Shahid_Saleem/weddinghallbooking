import UIKit

class SelectedTimeTbCell: UITableViewCell {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var allSalotsArray = [String]()
    var availableArry = [String]()
    var selectedIndex : Int = -1
    var timeselect:((_ time:String?) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTbl()
        print("all solts data =====\(allSalotsArray)")
    }
    func setupTbl(){
        collectionView.dataSource = self
        collectionView.delegate = self
        CVCells.addNibs(collectionView)
        self.availableArry.removeAll()
        let time =  UserDefaults.standard.string(forKey: "timeadd") ?? ""
        self.availableArry.append(time)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
extension SelectedTimeTbCell: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allSalotsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCells.TimeCVCell.identifier, for: indexPath) as! TimeCVCell
        let item = allSalotsArray[indexPath.item]
//        if  availableArry.contains(item){
//            cell.uiview.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.6901960784, blue: 0.9294117647, alpha: 0.19)
//            cell.lblTime.textColor = .white
            cell.lblTime.text = item
            if self.selectedIndex == indexPath.item {
                cell.uiview.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.7098039216, blue: 0.1058823529, alpha: 1)
                cell.lblTime.textColor = .white
            }else{
                cell.uiview.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.6901960784, blue: 0.9294117647, alpha: 0.19)
            }
//        }else{
//            cell.uiview.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.01176470588, blue: 0.3450980392, alpha: 1)
//            cell.lblTime.textColor = .white
//            cell.lblTime.text = item
//        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        let myTime = self.allSalotsArray[indexPath.item]
        self.timeselect?(myTime)
        self.collectionView.reloadData()
    }
}

extension SelectedTimeTbCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width/2 - 5
        let height = collectionView.bounds.size.height/2 - 5
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
