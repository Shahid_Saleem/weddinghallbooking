import UIKit

class SelectionServiceTbCell: UITableViewCell {

    @IBOutlet weak var selectionBtn: UIButton!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    var onClickSelection:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    @IBAction func selectinAction(_ sender: Any) {
        self.onClickSelection?()
    }
}
