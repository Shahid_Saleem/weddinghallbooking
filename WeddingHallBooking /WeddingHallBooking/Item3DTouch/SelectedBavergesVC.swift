import UIKit

class SelectedBavergesVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    
    var person:String = ""
    var selectedArr = ["Minerals Water","Cold drinks"]
    var arrPrice = ["40","50"]
    
    var selectedPrice:Double = 0.0
    var selectedServicesArr = [String]()
    var selectedFoodArr = [String]()
    
    var onClickValue:((_ item:String,_ price:String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTbl()
        
    }
    func setupTbl(){
        tableView.dataSource = self
        tableView.delegate = self
        TVCells.addNibs(tableView)
        
    }
    @IBAction func doneAction(_ sender: Any) {
        if self.selectedServicesArr.count == 0 {
            showErrorMessage(error: "Select Beverages ")
        }else{
            self.total()
        }
    }
    func total(){
        let selectedArray = self.selectedServicesArr.map { (itemd) -> String in
            return itemd
        }
        let selectedArrayfood = self.selectedFoodArr.map { (itemd) -> String in
            return itemd
        }
        let stringRepresentation = selectedArrayfood.joined(separator: ",")
        
        let strings = selectedArray
        let doubles = strings.compactMap(Double.init)
        let sum = doubles.reduce(0, +)
        let per = Double(self.person) ?? 0.0
        let total = "\(sum * per)"
        self.dismiss(animated: true,completion: {
            self.onClickValue?(stringRepresentation,total)
        })
        print(stringRepresentation)
        print(total)
    }
}
extension SelectedBavergesVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPrice.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TVCells.SelectionServiceTbCell.identifier) as! SelectionServiceTbCell
        let item = arrPrice[indexPath.row]
        let itemname = selectedArr[indexPath.row]
        cell.lblService.text = itemname
        cell.lblPrice.text = "RS \(item)"
        if selectedFoodArr.contains(where: { (items) -> Bool in
            return items == itemname
        }){
            print("ok")
        }
        if selectedServicesArr.contains(where: { (items) -> Bool in
            return items == item
        }){
            cell.selectionBtn.setImage(UIImage(named: "iconselection"), for: .normal)
        }else{
            cell.selectionBtn.setImage(UIImage(named: "selection"), for: .normal)
        }
        cell.layoutSubviews()
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let price = self.arrPrice[indexPath.row]
        let food = self.selectedArr[indexPath.row]
        self.selectedPrice += Double(price) ?? 0.0
        print("my selected price ==== \(selectedPrice)")
        if selectedServicesArr.contains(where: { (items) -> Bool in
            return items == price
        }){
            self.selectedServicesArr.removeAll { (itrms) -> Bool in
                return itrms == price
            }
        }else{
            self.selectedServicesArr.append(price)
        }
        if selectedFoodArr.contains(where: { (items) -> Bool in
            return items == food
        }){
            self.selectedFoodArr.removeAll { (itrms) -> Bool in
                return itrms == food
            }
        }else{
            self.selectedFoodArr.append(food)
        }
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let price = self.arrPrice[indexPath.row]
        self.selectedPrice -= Double(price) ?? 0.0
        tableView.reloadData()
    }
}
