import UIKit
import Kingfisher
import FSCalendar
import Frames
import Firebase
import DropDown
import KRProgressHUD

class Item3DTouchView: UIViewController,CardViewControllerDelegate,FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance{
    
    var somedays : Array = [String]()
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        return formatter
    }()
    
    
    @IBOutlet var viewMain: UIView!
    @IBOutlet var calendarView : FSCalendar!
    @IBOutlet weak var ratingStars: CosmosView!
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    @IBOutlet weak var lblBaverges: UILabel!
    @IBOutlet weak var seetingCapacity: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var food: UILabel!
    @IBOutlet weak var desert: UILabel!
    @IBOutlet weak var carParking: UILabel!
    @IBOutlet weak var service: UILabel!
    @IBOutlet var paymentBtn:UIButton!
    
    let dropDown = DropDown()
    var hallTypeDatas: hallTypeData?
    let checkoutAPIClient = CheckoutAPIClient(publicKey: "pk_test_6e40a700-d563-43cd-89d0-f9bb17d35e73",
                                              environment: .sandbox)
    let user = User.currentUser
    let hallId =  UserDefaults.standard.value(forKey: "hallId")
    let hallNamee =  UserDefaults.standard.value(forKey: "hallNamee")
    let hallImage =  UserDefaults.standard.value(forKey: "hallImage")
    let hallAddress =  UserDefaults.standard.value(forKey: "hallAddress")
    let hallRate =  UserDefaults.standard.value(forKey: "hallRate")
    let halltime =  UserDefaults.standard.value(forKey: "halltime")
    var reservationDate = ""
    var timeSlot = ""
    //UserDefaults.standard.value(forKey: "date")
    var event = ""
    var pricecal = ""
    //UserDefaults.standard.value(forKey: "bookingDate") ?? ""
    var hallFood = ""
    //UserDefaults.standard.value(forKey: "food")
    let hallDesert =  UserDefaults.standard.value(forKey: "desert")
    let hallSeatingCapacity =  UserDefaults.standard.value(forKey: "seatingCapacity")
    let hallCarParking =  UserDefaults.standard.value(forKey: "carParking")
    let hallService =  UserDefaults.standard.value(forKey: "service")
    let hallPrice = UserDefaults.standard.value(forKey: "price")
    var type:String = ""
    var foodTotal:String = ""
    var desertTotal:String = ""
    var halPrice:Int = 0
    var halPrice2:Int = 0
    var selectedperson:String = ""
    var selectedDesert:String = ""
    var selectedBevreges:String = ""
    var onClicvalueChange:((_ str:String)->Void)?
    
    
    @IBOutlet weak var lblAlert: UILabel!
    
    var PselectedArr = ["Beef Pulao","Egg Fried Rice","Kabli Pulao","Ch. Biryani","Mutton Biryani","Ch. Roast","Mutton Karhai","Mutton White Handi","Ch. Achari Handi","Ch. Karhai","Malai Boti","Seekh Kabab","Reshmi Kabab","Sag Paneer","Special Soup","Raita","Naan","Salad","Ice Cream"]
    var ParrPrice = ["240","160","200","180","220","170","330","310","260","240","140","100","120","80","90","20","25","40","60"]
    
    var PselectedArr2 = ["Beef Pulao","Egg Fried Rice","Kabli Pulao","Ch. Biryani","Mutton Biryani","Ch. Roast","Mutton Karhai","Mutton White Handi","Ch. Achari Handi","Ch. Karhai","Malai Boti","Seekh Kabab","Reshmi Kabab","Sag Paneer","Special Soup","Raita","Naan","Salad","Ice Cream"]
    var ParrPrice2 = ["250","150","200","180","220","160","350","320","260","240","140","120","100","80","90","20","25","40","60"]
    
    var GselectedArr = ["Beef Pulao","Egg Fried Rice","Kabli Pulao","Ch. Biryani","Mutton Biryani","Ch. Roast","Mutton Karhai","Mutton White Handi","Ch. Achari Handi","Ch. Karhai","Malai Boti","Seekh Kabab","Reshmi Kabab","Sag Paneer","Special Soup","Raita","Naan","Salad","Ice Cream"]
    var GarrPrice = ["220","150","180","200","220","160","270","210","260","240","130","120","100","80","90","20","25","40","60"]
    
    var SselectedArr = ["Beef Pulao","Egg Fried Rice","Kabli Pulao","Ch. Biryani","Mutton Biryani","Ch. Roast","Mutton Karhai","Mutton White Handi","Ch. Achari Handi","Ch. Karhai","Malai Boti","Seekh Kabab","Reshmi Kabab","Sag Paneer","Special Soup","Raita","Naan","Salad","Ice Cream"]
    var SarrPrice = ["200","140","160","185","210","145","250","180","230","220","130","110","100","80","70","20","25","40","50"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Event Colr
        calendarView.appearance.todayColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        calendarView.appearance.selectionColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        //calendarView.appearance.eventSelectionColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        // calendarView.select("2022-09-24 00:00:00 +0000"
        self.calendarView.appearance.eventDefaultColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        self.calendarView.appearance.eventSelectionColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //End
        
        getReservationData()
        title = "\(type) Hall Details"
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .never
        loadData()
        calendarView.delegate = self
        calendarView.dataSource = self
        checkOutSetup()
        if type == "Platinum"{
            self.dropDown.dataSource = ["100","150","200","300","400","500","600","800","1000","1200","1400","1500","1600","1800","2000"]
            KRProgressHUD.show()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3){
                if self.hallAddress as? String ?? ""  == "University Town" {
                    self.lblAlert.text = "Above 500 guest hall charges will be deducted"
               }else{
                   self.lblAlert.text = "Above 400 guest hall charges will be deducted"
               }
                KRProgressHUD.dismiss()
            }
        }else if type ==  "Golden"{
            self.dropDown.dataSource = ["100","150","200","300","400","500","600","800","1000","1200","1400","1500"]
            self.lblAlert.text = "Above 300 guest hall charges will be deducted"
        }else if type ==  "Silver"{
            self.dropDown.dataSource = ["100","150","200","300","400","500","600","800","1000"]
            self.lblAlert.text = "Above 200 guest hall charges will be deducted"
        }
        self.dropDown.direction = .bottom
        self.dropDown.dismissMode = .automatic
        self.dropDown.bottomOffset = CGPoint(x: 0, y:40)
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.seetingCapacity.text =  "Select Number of guests : " + item
            let selPer:Int = Int(item) ?? 0
            self.selectedperson = item
            if type == "Platinum"{
                if self.hallAddress as? String ?? ""  == "University Town"{
                    if selPer >= 400{
                        self.halPrice2 = 0
                    }else{
                        self.halPrice2 = self.halPrice
                    }
                }else{
                    if selPer >= 500{
                        self.halPrice2 = 0
                    }else{
                        self.halPrice2 = self.halPrice
                    }
                }
                
            }else if type ==  "Golden"{
                if selPer >= 300{
                    self.halPrice2 = 0
                }else{
                    self.halPrice2 = self.halPrice
                }
            }else if type ==  "Silver"{
                if selPer >= 200{
                    self.halPrice2 = 0
                }else{
                    self.halPrice2 = self.halPrice
                }
            }
        }
        self.onClicvalueChange = { value in
            let f = Double(self.foodTotal)
            let d = Double(self.desertTotal)
            let fint = Int(f ?? 0.0)
            let dint = Int(d ?? 0.0)
            let tr = (fint + dint)
            let trint = Int(tr)
            let trsint = Double(value)
            let trdint = Int(trsint ?? 0)
            let total = (trint + (trdint))
            let totalHall = (total + self.halPrice2)
            print("Final Price  ==   \(totalHall)")
            self.price.text = "Total Hall Charges: \(totalHall)"
        }
    }
    
    func update(_ rating: Double) {
        ratingStars.rating = rating
    }
    // MARK: - Data methods
    func loadData() {
        if let data = UserDefaults.standard.data(forKey: "hallDataDetails") {
            do {
                let decoder = JSONDecoder()
                let hallsData = try decoder.decode(hallTypeData.self, from: data)
                let url = URL(string: hallsData.hallImage ?? "")
                if url != nil {
                    imageProduct.loadGif(url: url, placeholder: PlaceHolder.icPlaceholder, mode: .scaleAspectFill)
                }else{
                    imageProduct.image = UIImage(named: "hall")
                }
                if hallsData.hallAddress ?? ""  == "University Town"{
                    labelTitle.text = "Welcome to " + "Town Wedding Hall"
                }else{
                    labelTitle.text = "Welcome to " + hallsData.hallName
                }
                print("hall anme -=--== \(hallsData.hallName ?? "")")
                labelAddress.text = "Address : " + hallsData.hallAddress
                seetingCapacity.text = "Select Number of guests : " + hallsData.seatingCapacity
                price.text = "Hall Charges " + hallsData.price
                self.halPrice = Int(hallsData.price) ?? 0
                carParking.text = "Number of car to park : " + hallsData.carParking
                food.text = "Select Food Items : "
                desert.text = "Select Desert Optional : " + self.selectedDesert
                //+ hallsData.desert
                service.text = "Services : " + hallsData.service
                lblBaverges.text = "Select Beverages :"
                ratingStars.isUserInteractionEnabled = false
                update(hallsData.hallRate ?? 0.0)
                
                UserDefaults.standard.set(hallsData.hallId, forKey: "hallId")
                UserDefaults.standard.set(hallsData.hallName, forKey: "hallNamee")
                UserDefaults.standard.set(hallsData.hallImage, forKey: "hallImage")
                UserDefaults.standard.set(hallsData.hallAddress, forKey: "hallAddress")
                UserDefaults.standard.set(hallsData.hallRate, forKey: "hallRate")
                UserDefaults.standard.set(hallsData.price, forKey: "price")
                UserDefaults.standard.set(hallsData.food, forKey: "food")
                UserDefaults.standard.set(hallsData.service, forKey: "service")
                UserDefaults.standard.set(hallsData.desert, forKey: "desert")
            }catch{
                print(error)
            }
        }
    }
    let date = Date()
    
    @IBAction func selectfoodItemAction(_ sender: Any) {
        if self.selectedperson.isEmpty{
            showErrorMessage(error: "Verify Number of Guests!")
        }else{
            if type == "Platinum"{
                let vc = self.getVCApp(storyboard: .Home, vcIdentifier: .SelectedFoodItemsVC) as! SelectedFoodItemsVC
                if self.hallAddress as? String ?? ""  == "University Town" {
                   vc.selectedArr = self.PselectedArr2
                   vc.arrPrice = self.ParrPrice2
               }else{
                   vc.selectedArr = self.PselectedArr
                   vc.arrPrice = self.ParrPrice
               }
                vc.person = self.selectedperson
                vc.type = self.type
                vc.onClickValue = { food , price in
                    self.food.text = "Food Items : " + food
                    self.hallFood = food
                    let valu = Double(price)
                    let str = Int(valu ?? 0.0)
                    self.pricecal =  "\(str)"
                    self.foodTotal = self.pricecal
                }
                self.presentVC(vc: vc,isAnimated: true,showAsPopUp:true)
            }else if type ==  "Golden"{
                let vc = self.getVCApp(storyboard: .Home, vcIdentifier: .SelectedFoodItemsVC) as! SelectedFoodItemsVC
                vc.person = self.selectedperson
                vc.selectedArr = self.GselectedArr
                vc.arrPrice = self.GarrPrice
                vc.type = self.type
                vc.onClickValue = { food , price in
                    self.food.text = "Food Items : " + food
                    self.hallFood = food
                    let valu = Double(price)
                    let str = Int(valu ?? 0.0)
                    self.pricecal =  "\(str)"
                    self.foodTotal = self.pricecal
                }
                self.presentVC(vc: vc,isAnimated: true,showAsPopUp:true)
            }else if type ==  "Silver"{
                let vc = self.getVCApp(storyboard: .Home, vcIdentifier: .SelectedFoodItemsVC) as! SelectedFoodItemsVC
                vc.person = self.selectedperson
                vc.selectedArr = self.SselectedArr
                vc.arrPrice = self.SarrPrice
                vc.type = self.type
                vc.onClickValue = { food , price in
                    self.food.text = "Food Items : " + food
                    self.hallFood = food
                    let valu = Double(price)
                    let str = Int(valu ?? 0.0)
                    self.pricecal =  "\(str)"
                    self.foodTotal = self.pricecal
                }
                self.presentVC(vc: vc,isAnimated: true,showAsPopUp:true)
            }
        }
    }
    @IBAction func dropDownAction(_ sd:UIButton) {
        self.dropDown.anchorView = sd
        self.dropDown.show()
    }
    
    @IBAction func desertAction(_ sender: UIButton) {
        if self.selectedperson.isEmpty{
            showErrorMessage(error: "Verify Number of Guests!")
        }else{
            let vc = self.getVCApp(storyboard: .Home, vcIdentifier: .SelectedDesertVC) as! SelectedDesertVC
            vc.person = self.selectedperson
            vc.onClickValue = { food , price in
                self.desert.text = "Select Desert Optional : \(food)"
                self.selectedDesert = food
                let pr = Double(price)
                let intpr = Int(pr ?? 0.0)
                self.desertTotal = "\(intpr)"
                print("Desert --- \(pr ?? 0)")
            }
            self.presentVC(vc: vc,isAnimated: true,showAsPopUp:true)
        }
    }
    
    @IBAction func beverges(_ sender: UIButton) {
        if self.selectedperson.isEmpty{
            showErrorMessage(error: "Verify Number of Guests!")
        }else{
            let vc = self.getVCApp(storyboard: .Home, vcIdentifier: .SelectedBavergesVC) as! SelectedBavergesVC
            vc.person = self.selectedperson
            vc.onClickValue = { food , price in
                self.lblBaverges.text = "Select Beverages : \(food)"
                self.selectedBevreges = food
                let pr = Double(price)
                let intpr = Int(pr ?? 0.0)
                self.onClicvalueChange?("\(pr ?? 0)")
                print("Beverages --- \(pr ?? 0)")
            }
            self.presentVC(vc: vc,isAnimated: true,showAsPopUp:true)
        }
    }
    @IBAction func paymentBtn(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let datetoday = (dateFormatter.string(from: date))
        if hallFood.isEmpty {
            self.showErrorMessage(error: "Select Food Items!")
        }else if self.selectedBevreges.isEmpty {
            self.showErrorMessage(error: "Please Select Beverages")
        }else if self.reservationDate.isEmpty {
            self.showErrorMessage(error: "Please Select date!")
        }else if self.timeSlot.isEmpty {
            self.showErrorMessage(error: "Please Select date & Time")
        }else if datetoday >= self.reservationDate {
            self.showErrorMessage(error: "Please Select after date!")
        }else{
//            if self.reservationDate == event {
//                self.showErrorMessage(error: "Booking is already Reserved")
//            }else{
                UserDefaults.standard.set(self.timeSlot, forKey: "timeadd")
//                let filtersCategoryView = cardViewController
//                self.navigationController?.pushViewController(filtersCategoryView, animated: true)
            self.paymentDone()
           // }
            print("event date \(event)")
        }
    }
    
    private func getReservationData(){
        if let uid = getCurrentUserId() {
            getRersevationOfClient(
                byId: uid,
                success: { [weak self] currentReservation in
                    Reservation.currentReservation = currentReservation
                    self?.event = "\(currentReservation?.reservationDate ?? "")"
                    self?.appointData.append(self?.event ?? "")
                    self?.setupDates(mDataDates: self?.mDates ?? [Date]())
                },
                failure: { [weak self] in
                    self?.showErrorMessage(error: "Failed get data")
                }
            )
        }else{
            self.showErrorMessage(error: "Failed get data")
        }
    }
    //MARK: - Payment CheckOUt Setup
    private func checkOutSetup(){
        cardViewController.delegate = self
        cardViewController.rightBarButtonItem = UIBarButtonItem(title: "Pay", style: .done, target: nil, action: nil)
        cardViewController.availableSchemes = [.visa,.mastercard]
        cardViewController.setDefault(regionCode: "PK")
    }
    override func viewDidAppear(_ animated: Bool) {
        cardViewController.addressViewController.setCountrySelected(country: "PK", regionCode: "PK")
    }
    lazy var cardViewController: CardViewController = {
        let b = CardViewController(checkoutApiClient: checkoutAPIClient, cardHolderNameState: .normal, billingDetailsState: .required, defaultRegionCode: "PK")
        b.billingDetailsAddress = CkoAddress(addressLine1: "Peshawar Khyber Pakhtunkhwa", addressLine2: "Peshawar Khyber Pakhtunkhwa", city: "Peshawar", state: "Khyber Pakhtunkhwa", zip: "25120", country: "PK")
        b.billingDetailsPhone = CkoPhoneNumber(countryCode: "92", number: "\(user?.phoneNumber ?? "")")
        b.delegate = self
        b.addressViewController.setFields(address: b.billingDetailsAddress!, phone: b.billingDetailsPhone!)
        return b
    }()
    func onTapDone(controller: CardViewController, cardToken: CkoCardTokenResponse?, status: CheckoutTokenStatus) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        switch status {
        case .success:
            self.paymentDone()
        case .failure:
            self.paymentDone()
            print("failure")
        }
    }
    func onSubmit(controller: CardViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func paymentDone(){
        var hal = ""
        if let uid = UserDefaults.standard.string(forKey: "uid") {
            if self.hallAddress as! String  == "University Town"{
                hal = "Town Wedding Hall"
            }else{
                hal = self.hallNamee as Any as! String
            }
            let data: [String:Any] = ["name" :hal,"hallId": self.hallId ?? 0, "address": self.hallAddress ?? "", "id": uid, "reservationDate": "\(self.reservationDate) \(self.timeSlot)",  "clientId" : (User.currentUser?.id!)! , "rate" : self.hallRate ?? "","imageUrl":self.hallImage ?? "","service":self.hallService ?? "","food":self.hallFood,"desert" : "\(self.selectedDesert) \(self.selectedBevreges)" ,"price" : self.price.text ?? ""]
            Reservation.currentReservation = parseReservationData(jsonReservation: data)
            setReservation(id: uid, reservationData: data)
        }else{
            let alert = UIAlertController(title: "Error", message: "Faild to Reserve", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Okay", style: .default) { UIAlertAction in
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        let alert = UIAlertController(title: "Confirmation", message: "You have Successfully booked \(type) Hall,\nHall Name \(hal) on this Date:\(self.reservationDate)\n\(self.timeSlot)\n Please pay your payment a day before the event ", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Done", style: .default) { UIAlertAction in
            // UserDefaults.standard.set(self.reservationDate, forKey: "bookingDate")
            self.jumpToHomeView()
            //self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    private func jumpToHomeView(){
        let home = Home()
        let navigation = NavigationController(rootViewController: home)
        navigation.isModalInPresentation = true
        navigation.navigationBar.isTranslucent = false
        navigation.modalPresentationStyle = .fullScreen
        present(navigation, animated: true)
    }
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        // print("numberOfEventsFor \(mDataDates)")
        for separatedate in mDataDates {
            //  print("separatedate... \(separatedate)")
            if self.mDataDates.contains(separatedate) {
                return 1
            }
        }
        return 0
    }
    
    func setupDates(mDataDates : [Date]){
        self.mDataDates = mDataDates
        self.calendarView.reloadData()
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let vc = self.getVCApp(storyboard: .Home, vcIdentifier: .SelectDateTimeVC) as! SelectDateTimeVC
        vc.onClickValue = { time in
            self.timeSlot = time
            UserDefaults.standard.removeObject(forKey: "timeSlots")
            UserDefaults.standard.set(time, forKey: "timeSlots")
            print("Slot time --- \(time)")
        }
        self.presentVC(vc: vc,isAnimated: true,showAsPopUp:true)
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let showDate = inputFormatter.date(from: "\(date)")
        inputFormatter.dateFormat = "MM-dd-yyyy"
        let resultString = inputFormatter.string(from: showDate ?? Date())
        self.reservationDate = resultString
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let showDate = inputFormatter.date(from: "\(date)")
        inputFormatter.dateFormat = "MM-dd-yyyy"
        
        let dateString = inputFormatter.string(from: date)
        //        print(" \(dateString)")
        if self.reservationDate.contains(dateString) {
            return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        return calendarView.appearance.selectionColor
    }
    
    private func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventColorFor date: Date) -> UIColor? {
        //Do some checks and return whatever color you want to.
        return UIColor.black
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        print("eventDefaultColorsFor \(date)")
        for separatedate in mDataDates {
            var dateseparte = Date()
            dateseparte = separatedate
            print("dateseparte \(dateseparte)")
            if self.gregorian.isDateInToday(date) {
                return [UIColor.orange]
            }
        }
        return [UIColor.clear]
    }
    
    var mDataDates = [Date]()
    var appointData = [String]()
    var mDates : [Date] {
        var date = [Date]()
        appointData.forEach { (items) in
            let dateObj = (items).getDate(inputFormat: "MM-dd-yyyy")
            date.append(dateObj)
        }
        return date
    }
}
