import UIKit

class SelectDateTimeVC: BaseVC {
    
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var tabelView: UITableView!
    
    var allSlotsArray = ["11:30 Am To 03:00 Pm","08:00 Pm To 12:00 Am","04:00 Pm To 7:30 Pm"]
    var mData = [confermServicesField]()
    var onCalBack:(() -> Void)?
    var date = "",time = ""
    let dateCurrent = Date()
    let formatter = DateFormatter()
    var onClickValue:((_ time:String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiView.newRoundCornersView([.topLeft,.topRight], radius: 15.0)
        setupTbl()
    }
    func setupTbl(){
        formatter.dateFormat = "YYYY-MM-dd"
        let result = formatter.string(from: dateCurrent)
        tabelView.dataSource = self
        tabelView.delegate = self
        TVCells.addNibs(tabelView)
        self.mData.removeAll()
        self.mData.append(contentsOf: confermServicesField.getData())
        self.date = result
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SelectDateTimeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let item = self.mData[indexPath.item]
        switch item.item {
        case .time:
            cell = tableView.dequeueReusableCell(withIdentifier: TVCells.SelectedTimeTbCell.identifier) as! SelectedTimeTbCell
            if let cell = cell as? SelectedTimeTbCell {
                cell.allSalotsArray = self.allSlotsArray
                cell.timeselect = { value in
                    self.time = value ?? ""
                }
                cell.collectionView.reloadData()
            }
        case .conferm:
            cell = tableView.dequeueReusableCell(withIdentifier: TVCells.buttonSignUpCell.identifier) as! buttonSignUpCell
            if let cell = cell as? buttonSignUpCell {
                cell.onClickMe = {
                    if self.time.isEmpty {
                        self.showErrorMessage(error: "Select Time!")
                    }else{
                        self.dismiss(animated: true,completion: {
                            self.onClickValue?(self.time)
                        })
                    }
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
}
class confermServicesField {
    var value:String = ""
    var item:ServicesBook = .time
    static func getData() -> [confermServicesField]{
        var list = [confermServicesField]()
        for items in ServicesBook.allCases {
            let item = confermServicesField()
            item.item = items
            list.append(item)
        }
        return list
    }
}
enum ServicesBook:String,CaseIterable {
    
    case time = "time"
    case conferm = "Book"
    
}

extension UIView {
    func newRoundCornersView(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11, *) {
            var cornerMask = CACornerMask()
            if(corners.contains(.topLeft)){
                cornerMask.insert(.layerMinXMinYCorner)
            }
            if(corners.contains(.topRight)){
                cornerMask.insert(.layerMaxXMinYCorner)
            }
            if(corners.contains(.bottomLeft)){
                cornerMask.insert(.layerMinXMaxYCorner)
            }
            if(corners.contains(.bottomRight)){
                cornerMask.insert(.layerMaxXMaxYCorner)
            }
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = cornerMask
            
        }else {
            
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}
