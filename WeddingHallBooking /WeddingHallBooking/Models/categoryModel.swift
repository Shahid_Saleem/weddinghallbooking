import Foundation
struct categorys :Codable{
    let id: String!
    var name: String!
    var hallsTypeData :hallTypeDatas
  init(withName name: String, id:String,hallsTypes: hallTypeDatas = [:]) {
        self.id = id
        self.name = name
        self.hallsTypeData = hallsTypes
    }

    static var categoryss:categorys? = nil
}

func parseCatogry(jsonCatogry:  [String : Any]?) -> categorys? {
    if jsonCatogry == nil {
        return nil
    }
  var hallsTypes:hallTypeDatas = [:]
  if let hallsTypesData = jsonCatogry!["hallTypeData"] as? Dictionary<String, Any> {
    hallsTypes = parseHallsTypesData(jsonHallTytpesData: hallsTypesData)
  }

    let categorys = categorys(withName: jsonCatogry?["hallTypeName"] as? String ?? "", id: jsonCatogry?["hallTypeId"] as? String ?? "",hallsTypes:hallsTypes)
            return categorys

}


func jsonCatogry(categorys: categorys) -> [String: Any] {
    var jsonCatogryys: [String: Any] = [:]
  jsonCatogryys["hallTypeId"] = categorys.id
  jsonCatogryys["hallTypeName"] = categorys.name
  jsonCatogryys["hallTypeData"] = jsonHallTypeData(hallstypess: categorys.hallsTypeData)


  return jsonCatogryys
}

struct hallTypeData :Codable{
    var hallName:String!
    var hallId: String!
    var hallAddress: String!
    var hallRate: Double!
    var hallImage : String!
    var food :String!
    var price :String!
    var carParking :String!
    var desert :String!
    var seatingCapacity :String!
    var service :String!

  init(hallName:String = "",hallId: String = "", hallAddress: String = "", hallRate: Double = 0.0,hallImage:String = "",price :String = "", food :String = "", desert :String = "", carParking :String = "",seatingCapacity:String = "",service:String = "") {
        self.hallName = hallName
        self.hallId = hallId
        self.hallAddress = hallAddress
        self.hallRate = hallRate
        self.hallImage  = hallImage
        self.seatingCapacity = seatingCapacity
        self.price = price
        self.food = food
        self.carParking = carParking
        self.desert = desert
        self.service = service
    }
}

typealias hallTypeDatas = Dictionary<String, hallTypeData>

func parseHallsTypesData(jsonHallTytpesData: [String: Any]) -> hallTypeDatas {
    var hallsTypesData: hallTypeDatas = [:]
    for (_, hallstypes) in jsonHallTytpesData.enumerated() {
        if let hallstype = hallstypes.value as? Dictionary<String, Any> {
          hallsTypesData[hallstypes.key] = hallTypeData(hallName:hallstype["hallName"] as? String ?? "",hallId: hallstype["hallId"] as? String ?? "", hallAddress: hallstype["hallAddress"] as? String ?? "", hallRate: hallstype["hallRate"] as? Double ?? 0.0 ,hallImage : hallstype["hallImage"] as? String ?? "",price: hallstype["price"] as? String ?? "" ,food: hallstype["food"] as? String ?? "", desert: hallstype["desert"] as? String ?? "" ,carParking:hallstype["carParking"] as? String ?? "" , seatingCapacity: hallstype["seatingCapacity"] as? String ?? "",service: hallstype["service"] as? String ?? "" )
        }
    }
    return hallsTypesData
}

func jsonHallTypeData(hallstypess: hallTypeDatas) -> [String: [String: Any]] {
    var jsonHallsTypesData: [String: [String: Any]] = [:]
    for (_, hallstypes) in hallstypess.enumerated() {
      jsonHallsTypesData[hallstypes.key] = [:]
      jsonHallsTypesData[hallstypes.key]!["hallName"] = hallstypes.value.hallName
      jsonHallsTypesData[hallstypes.key]!["hallId"] = hallstypes.value.hallId
      jsonHallsTypesData[hallstypes.key]!["hallAddress"] = hallstypes.value.hallAddress
      jsonHallsTypesData[hallstypes.key]!["hallRate"] = hallstypes.value.hallRate
      jsonHallsTypesData[hallstypes.key]!["hallImage"] = hallstypes.value.hallImage
      jsonHallsTypesData[hallstypes.key]!["price"] = hallstypes.value.price
      jsonHallsTypesData[hallstypes.key]!["food"] = hallstypes.value.food
      jsonHallsTypesData[hallstypes.key]!["carParking"] = hallstypes.value.carParking
      jsonHallsTypesData[hallstypes.key]!["seatingCapacity"] = hallstypes.value.seatingCapacity
      jsonHallsTypesData[hallstypes.key]!["desert"] = hallstypes.value.desert
      jsonHallsTypesData[hallstypes.key]!["service"] = hallstypes.value.service
    }
    return jsonHallsTypesData
}
