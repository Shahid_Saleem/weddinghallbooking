import Foundation

struct User {
    var id: String?
    var name: String?
    var email: String?
    var signUpDate: Date?
    var password:String?
    var phoneNumber:String?
    
    init(withName name: String, id:String, signUpDate: Date,email :String,password:String,phoneNumber:String) {
        self.id = id
        self.name = name
        self.email = email
        self.signUpDate = signUpDate
        self.password = password
        self.phoneNumber = phoneNumber
    }
    static var currentUser:User? = nil
}

func parseUser(jsonUser:  [String : Any]?) -> User? {
    if jsonUser == nil {
        return nil
    }
    let signUpDate = TimeFormatHelper.date(from: jsonUser!["signUpDate"] as? String ?? "", format: "MM-dd-yyyy") ?? Date()
    let user = User(withName: jsonUser!["name"] as! String, id: jsonUser!["id"] as! String, signUpDate: signUpDate,email: jsonUser!["email"] as! String,password: jsonUser!["password"] as! String,phoneNumber: jsonUser!["phoneNumber"] as? String ?? "")
    return user
}

func jsonUser(user: User) -> [String: Any] {
    var jsonUser: [String: Any] = [:]
    jsonUser["id"] = user.id
    jsonUser["name"] = user.name
    jsonUser["signUpDate"] = TimeFormatHelper.string(for:user.signUpDate ?? Date(), format:"MM-dd-yyyy")
    jsonUser["email"] = user.email
    jsonUser["password"] = user.password
    jsonUser["phoneNumber"] = user.phoneNumber
    
    return jsonUser
}
