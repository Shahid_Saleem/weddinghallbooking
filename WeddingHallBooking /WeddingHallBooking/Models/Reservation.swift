import Foundation


struct Reservation {
    var reservationDate:String?
    var id: String?
    var name: String?
    var address: String?
    var rate: Double?
    var clientId:String?
    var imageUrl : String?
    var food:String?
    var desert: String?
    var service: String?
    var price: String?


  init(withName name: String, id:String, address: String,rate :Double,clientId:String,reservationDate:String,imageUrl :String,service:String,food:String,desert:String,price:String) {
        self.id = id
        self.name = name
        self.address = address
        self.rate = rate
        self.clientId = clientId
        self.reservationDate = reservationDate
        self.imageUrl = imageUrl
        self.price = price
        self.food = food
        self.desert = desert
        self.service = service
      }

    static var currentReservation:Reservation? = nil
}




func parseReservationData(jsonReservation:  [String : Any]?) -> Reservation? {
    if jsonReservation == nil {
        return nil
    }



  let reservation = Reservation(withName: jsonReservation!["name"] as! String, id: jsonReservation!["hallId"] as! String, address: jsonReservation!["address"] as! String,rate: jsonReservation!["rate"] as! Double,clientId: jsonReservation!["clientId"] as! String,reservationDate : jsonReservation!["reservationDate"] as? String ?? "",imageUrl: jsonReservation!["imageUrl"] as? String ?? "",service: jsonReservation!["service"] as? String ?? "" ,food: jsonReservation!["food"] as? String ?? "" , desert : jsonReservation!["desert"] as? String ?? "", price : jsonReservation!["price"] as? String ?? "")
            return reservation
        }



func jsonReservation(reservation: Reservation) -> [String: Any] {
    var jsonReservation: [String: Any] = [:]
    jsonReservation["hallId"] = reservation.id
    jsonReservation["name"] = reservation.name
    jsonReservation["reservationDate"] = reservation.reservationDate
    jsonReservation["address"] = reservation.address
    jsonReservation["clientId"] = reservation.clientId
    jsonReservation["rate"] = reservation.rate
    jsonReservation["imageUrl"] = reservation.imageUrl
    jsonReservation["food"] = reservation.food
    jsonReservation["desert"] = reservation.desert
    jsonReservation["price"] = reservation.price
    jsonReservation["service"] = reservation.service
    return jsonReservation
}

